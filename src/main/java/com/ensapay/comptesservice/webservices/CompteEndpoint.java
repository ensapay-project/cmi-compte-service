package com.ensapay.comptesservice.webservices;

import cmi_comptes_service.*;
import com.ensapay.comptesservice.services.ComptesService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
@AllArgsConstructor
@Log4j2
public class CompteEndpoint {
    private final static String NAMESPACE_URL = "http://cmi-comptes-service";

    ComptesService service;

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "getAllComptesRequest")
    @ResponsePayload
    public GetAllComptesResponse getAllComptes(@RequestPayload GetAllComptesRequest request) {
        return service.getAllComptes(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "createCompteRequest")
    @ResponsePayload
    public CreateCompteResponse createCompte(@RequestPayload CreateCompteRequest request) {
        log.info("Compte : {}", request.getRequestBody().getBankId());
        return service.createCompte(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "updateCompteRequest")
    @ResponsePayload
    public CreateCompteResponse modifierCompte(@RequestPayload UpdateCompteRequest request) {
        return service.modifierCompte(request);
    }

    @PayloadRoot(namespace = NAMESPACE_URL, localPart = "deleteCompteByIdRequest")
    @ResponsePayload
    public void deleteCompte(@RequestPayload DeleteCompteByIdRequest request) {
        service.deleteCompte(request);
    }

}
