package com.ensapay.comptesservice.entities;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.util.Date;

@Data
@NoArgsConstructor
//@Builder
public class User {
//    private String id;
    private Date createdTimestamp;
    private String email, numeroTel;
    private String firstName, lastName;
}
