package com.ensapay.comptesservice.entities;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
public class CompteEntity {
    @Id
    private String numeroCompte;
    private Double soldeFinal;
    private String clientId, intitule,bankId;

}
