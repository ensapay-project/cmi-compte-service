package com.ensapay.comptesservice.repositories;

import cmi_comptes_service.Compte;
import com.ensapay.comptesservice.entities.CompteEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ComptesRepository extends MongoRepository<CompteEntity,String> {
    List<CompteEntity> findAllByBankId(String bankId);

//    Optional<c> findByNumeroCompte(String numeroCompte);
}
