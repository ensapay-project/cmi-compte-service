package com.ensapay.comptesservice.services;

import cmi_comptes_service.*;
import com.ensapay.comptesservice.entities.CompteEntity;
import com.ensapay.comptesservice.repositories.ComptesRepository;
import com.ensapay.comptesservice.utils.NumeroCompteGenerator;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Log4j2
public class ComptesService {
    ComptesRepository repository;

    public GetAllComptesResponse getAllComptes(GetAllComptesRequest request) {
//        KeycloakAuthenticationToken principal = (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        GetAllComptesResponse response = new GetAllComptesResponse();

        List<CompteEntity> comptesList = repository.findAllByBankId(request.getBankId());
        List<Compte> comptes = comptesList.stream()
            .map(compteEntity -> {
                Compte compte = new Compte();
                compte.setIntitule(compteEntity.getIntitule());
                compte.setSoldeFinal(compteEntity.getSoldeFinal());
                compte.setClientId(compteEntity.getClientId());
                compte.setBankId(compteEntity.getBankId());
                compte.setNumeroCompte(compteEntity.getNumeroCompte());
                return compte;
            }).collect(Collectors.toList());

        response.getComptes().addAll(comptes);
//        if (principal.getAccount().getRoles().contains("ROLE_agent"))
//            response.getComptes().addAll(comptes);
//        else response.getComptes().addAll(
//            comptes.stream().filter(compte -> compte.getClientId().equals(principal.getName())).collect(Collectors.toList())
//        );
        return response;
    }

    public CreateCompteResponse createCompte(CreateCompteRequest request) {
        CompteEntity compte = new CompteEntity();
        CompteRequestBody requestBody = request.getRequestBody();
        compte.setBankId(requestBody.getBankId());
        compte.setClientId(requestBody.getClientId());
        compte.setIntitule(requestBody.getIntitule());
        compte.setSoldeFinal(requestBody.getSoldeFinal());
        compte.setNumeroCompte(NumeroCompteGenerator.generateMasterCardNumber());
        CreateCompteResponse response = new CreateCompteResponse();

        repository.save(compte);

        Compte responseCompte = new Compte();
        responseCompte.setBankId(compte.getBankId());
        responseCompte.setClientId(compte.getClientId());
        responseCompte.setIntitule(compte.getIntitule());
        responseCompte.setSoldeFinal(compte.getSoldeFinal());
        responseCompte.setNumeroCompte(compte.getNumeroCompte());

        response.setCompte(responseCompte);
        return response;
    }

    public CreateCompteResponse modifierCompte(UpdateCompteRequest request) {
        log.info("Numroe cmi-compte : {}", request.getRequestBody().getNumeroCompte());

        CompteEntity compte = repository.findById(request.getRequestBody().getNumeroCompte()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));

        if (request.getRequestBody().getSoldeFinal() > 0)
            compte.setSoldeFinal(request.getRequestBody().getSoldeFinal());

        if (request.getRequestBody().getIntitule() != null)
            compte.setIntitule(request.getRequestBody().getIntitule());

        repository.save(compte);
        CreateCompteResponse response = new CreateCompteResponse();
        Compte responseCompte = new Compte();
        responseCompte.setBankId(compte.getBankId());
        responseCompte.setClientId(compte.getClientId());
        responseCompte.setIntitule(compte.getIntitule());
        responseCompte.setSoldeFinal(compte.getSoldeFinal());
        responseCompte.setNumeroCompte(compte.getNumeroCompte());

        response.setCompte(responseCompte);

        return response;
    }

    public void deleteCompte(DeleteCompteByIdRequest request) {
        repository.delete(
            repository.findById(request.getNumeroCompte()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
        );
    }
}
