package com.ensapay.comptesservice.utils;

import org.apache.commons.lang.RandomStringUtils;

import java.util.Random;

public class RandomPasswordGenerator {
    public static String generate() {
        return RandomStringUtils.random(8);
    }
}
