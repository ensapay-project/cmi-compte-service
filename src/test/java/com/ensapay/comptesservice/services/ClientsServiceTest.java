//package com.ensapay.comptesservice.services;
//
//import com.ensapay.comptesservice.entities.User;
//import com.ensapay.comptesservice.feign.AuthServiceClient;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//
//import java.util.*;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//import static org.mockito.MockitoAnnotations.initMocks;
//
//class ClientsServiceTest {
//
//    @Mock
//    private ClientsRepository mockRepository;
//    @Mock
//    private AuthServiceClient mockAuthServiceClient;
//    @Mock
//    private MailService mockMailService;
//
//    private ClientsService clientsServiceUnderTest;
//
//    @BeforeEach
//    void setUp() {
//        initMocks(this);
//        clientsServiceUnderTest = new ClientsService(mockRepository, mockAuthServiceClient, mockMailService);
//    }
//
//    @Test
//    void testGetClients() {
//        // Setup
//        final Client client = new Client();
//        client.setUserId("userId");
//        client.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        client.setAccount(account);
//        final List<Client> expectedResult = Arrays.asList(client);
//
//        // Configure ClientsRepository.findAll(...).
//        final Client client1 = new Client();
//        client1.setUserId("userId");
//        client1.setNumeroTel("numeroTel");
//        final User account1 = new User();
//        account1.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account1.setEmail("email");
//        account1.setNumeroTel("numeroTel");
//        account1.setFirstName("firstName");
//        account1.setLastName("lastName");
//        client1.setAccount(account1);
//        final List<Client> clients = Arrays.asList(client1);
//        when(mockRepository.findAll()).thenReturn(clients);
//
//        // Run the test
//        final List<Client> result = clientsServiceUnderTest.getClients();
//
//        // Verify the results
//        assertThat(result).isEqualTo(expectedResult);
//    }
//
//    @Test
//    void testGetClient() {
//        // Setup
//        final Client client = new Client();
//        client.setUserId("userId");
//        client.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        client.setAccount(account);
//        final Optional<Client> expectedResult = Optional.of(client);
//
//        // Configure ClientsRepository.findById(...).
//        final Client client2 = new Client();
//        client2.setUserId("userId");
//        client2.setNumeroTel("numeroTel");
//        final User account1 = new User();
//        account1.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account1.setEmail("email");
//        account1.setNumeroTel("numeroTel");
//        account1.setFirstName("firstName");
//        account1.setLastName("lastName");
//        client2.setAccount(account1);
//        final Optional<Client> client1 = Optional.of(client2);
//        when(mockRepository.findById("id")).thenReturn(client1);
//
//        // Run the test
//        final Optional<Client> result = clientsServiceUnderTest.getClient("id");
//
//        // Verify the results
//        assertThat(result).isEqualTo(expectedResult);
//    }
//
//    @Test
//    void testGetClient_ClientsRepositoryReturnsAbsent() {
//        // Setup
//        final Client client = new Client();
//        client.setUserId("userId");
//        client.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        client.setAccount(account);
//        final Optional<Client> expectedResult = Optional.of(client);
//        when(mockRepository.findById("id")).thenReturn(Optional.empty());
//
//        // Run the test
//        final Optional<Client> result = clientsServiceUnderTest.getClient("id");
//
//        // Verify the results
//        assertThat(result).isEqualTo(expectedResult);
//    }
//
//    @Test
//    void testCreateClient() {
//        // Setup
//        final User user = new User();
//        user.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        user.setEmail("email");
//        user.setNumeroTel("numeroTel");
//        user.setFirstName("firstName");
//        user.setLastName("lastName");
//
//        final Client expectedResult = new Client();
//        expectedResult.setUserId("userId");
//        expectedResult.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        expectedResult.setAccount(account);
//
//        // Configure ClientsRepository.save(...).
//        final Client client = new Client();
//        client.setUserId("userId");
//        client.setNumeroTel("numeroTel");
//        final User account1 = new User();
//        account1.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account1.setEmail("email");
//        account1.setNumeroTel("numeroTel");
//        account1.setFirstName("firstName");
//        account1.setLastName("lastName");
//        client.setAccount(account1);
//        when(mockRepository.save(new Client())).thenReturn(client);
//
//        // Run the test
//        final Client result = clientsServiceUnderTest.createClient(user);
//
//        // Verify the results
//        assertThat(result).isEqualTo(expectedResult);
//        verify(mockMailService).sendPasswordRecoveryMail(new User(), "password");
//    }
//
//    @Test
//    void testResetPassword() {
//        // Setup
//        final Client client = new Client();
//        client.setUserId("userId");
//        client.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        client.setAccount(account);
//
//        // Run the test
//        clientsServiceUnderTest.resetPassword(client);
//
//        // Verify the results
//        verify(mockMailService).sendPasswordRecoveryMail(new User(), "password");
//    }
//
//    @Test
//    void testUpdateClient() {
//        // Setup
//        final User body = new User();
//        body.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        body.setEmail("email");
//        body.setNumeroTel("numeroTel");
//        body.setFirstName("firstName");
//        body.setLastName("lastName");
//
//        final Client expectedResult = new Client();
//        expectedResult.setUserId("userId");
//        expectedResult.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        expectedResult.setAccount(account);
//
//        // Configure ClientsRepository.findById(...).
//        final Client client1 = new Client();
//        client1.setUserId("userId");
//        client1.setNumeroTel("numeroTel");
//        final User account1 = new User();
//        account1.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account1.setEmail("email");
//        account1.setNumeroTel("numeroTel");
//        account1.setFirstName("firstName");
//        account1.setLastName("lastName");
//        client1.setAccount(account1);
//        final Optional<Client> client = Optional.of(client1);
//        when(mockRepository.findById("id")).thenReturn(client);
//
//        // Configure ClientsRepository.save(...).
//        final Client client2 = new Client();
//        client2.setUserId("userId");
//        client2.setNumeroTel("numeroTel");
//        final User account2 = new User();
//        account2.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account2.setEmail("email");
//        account2.setNumeroTel("numeroTel");
//        account2.setFirstName("firstName");
//        account2.setLastName("lastName");
//        client2.setAccount(account2);
//        when(mockRepository.save(new Client())).thenReturn(client2);
//
//        // Run the test
//        final Client result = clientsServiceUnderTest.updateClient("id", body);
//
//        // Verify the results
//        assertThat(result).isEqualTo(expectedResult);
//    }
//
//    @Test
//    void testUpdateClient_ClientsRepositoryFindByIdReturnsAbsent() {
//        // Setup
//        final User body = new User();
//        body.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        body.setEmail("email");
//        body.setNumeroTel("numeroTel");
//        body.setFirstName("firstName");
//        body.setLastName("lastName");
//
//        final Client expectedResult = new Client();
//        expectedResult.setUserId("userId");
//        expectedResult.setNumeroTel("numeroTel");
//        final User account = new User();
//        account.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account.setEmail("email");
//        account.setNumeroTel("numeroTel");
//        account.setFirstName("firstName");
//        account.setLastName("lastName");
//        expectedResult.setAccount(account);
//
//        when(mockRepository.findById("id")).thenReturn(Optional.empty());
//
//        // Configure ClientsRepository.save(...).
//        final Client client = new Client();
//        client.setUserId("userId");
//        client.setNumeroTel("numeroTel");
//        final User account1 = new User();
//        account1.setCreatedTimestamp(new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
//        account1.setEmail("email");
//        account1.setNumeroTel("numeroTel");
//        account1.setFirstName("firstName");
//        account1.setLastName("lastName");
//        client.setAccount(account1);
//        when(mockRepository.save(new Client())).thenReturn(client);
//
//        // Run the test
//        final Client result = clientsServiceUnderTest.updateClient("id", body);
//
//        // Verify the results
//        assertThat(result).isEqualTo(expectedResult);
//    }
//
//    @Test
//    void testDeleteClient() {
//        // Setup
//
//        // Run the test
//        clientsServiceUnderTest.deleteClient("id");
//
//        // Verify the results
//        verify(mockRepository).deleteById("id");
//    }
//}
